#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <dirent.h>

bool isVideoFormat(const char FILENAME[]);
char** inputNames();

int main(int argc, char* argv[]){

  printf("Maneki-neko version: 1.0\n");
  printf("------------------------\n");
  
  int i;
  char** input = inputNames();
  const int SIZE = sizeof(input) / sizeof(input[0]); 
  
  // freeing dynamically allocated memory
  for(i = 0; i < SIZE; i++){
    free(input[i]);  
  }
  free(input);

  return 0; 
}

bool isVideoFormat(const char FILENAME[]){
  
  const int N = 2;
  const int M = 5;
  const int SIZE = strlen(FILENAME);
  const char EXTS[2][5] = {".mkv",".mp4"};
  int i;
  int result = -1;
  char substr[M];

  strncpy(substr, FILENAME + SIZE - M + 1, 5);

  for(i = 0; i < N; i++){
    result = strcmp(substr, EXTS[i]);
    if(result == 0)
      return true;
  }
  
  return false;
}

char** inputNames(){

  const int N = 150; // number of maximum episodes per batch
  const int M = 100; // maximum length of characters per file name
  int i;
  int index = 0;
  char buffer[M];

  // allocating input array
  char **inputArr = malloc(N * sizeof(char *));
  if (inputArr == NULL){
    fprintf(stderr, "ERROR: failed to dynamically allocate inputArr"); exit(-1);
  }

  for (i = 0; i < N; i++){
    inputArr[i] = malloc(M * sizeof(char *));
  }
    
  // accessing current directory
  DIR* dir = opendir(".");
  if(dir == NULL){
    fprintf(stderr, "ERROR: failed to open directory"); exit(-1);
  }
  
  struct dirent* entity;
  entity = readdir(dir);

  while(entity != NULL){
    strcpy(buffer,entity->d_name);
    entity = readdir(dir);

    if(isVideoFormat(buffer)){
      strcpy(inputArr[index], buffer);
      index++;
    }
  }

  closedir(dir); 
  return inputArr;
}
